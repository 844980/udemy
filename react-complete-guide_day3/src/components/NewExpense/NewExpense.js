import ExpenseForm from './ExpenseForm';
import './NewExpense.css';
import {useState} from 'react';

const NewExpense = (props) => {
	
	const [isNew, setIsNew] = useState(false);
	const newHandler = () =>{
		setIsNew(true);
	};
	const cancelHandler = () =>{
		setIsNew(false);
	};
	const saveExpenseDataHandler = (data) => {
		const expenseData = {
			...data,
			id:Math.random().toString(),
		}
		props.onAddExpense(expenseData);
    	setIsNew(false);
	}
  return (
    <div className='new-expense'>
            {!isNew && (
        <button onClick={newHandler}>신규</button>
      )}
      {isNew && (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          onCancel={cancelHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;