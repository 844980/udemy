//import logo from './logo.svg';
import './App.css';
import Expenses from './components/Expenses/Expenses';
import NewExpense from './components/NewExpense/NewExpense'
import {useState} from 'react';

function App() {
	const [expenses, setExpenses] = useState([{
            id: 'e1',
            title: 'ㅋㅋ',
            amount: 94.12,
            date: new Date(2021, 7, 14),
        },
        {
            id: 'e2',
            title: 'ㅇㅇ',
            amount: 799.49,
            date: new Date(2022, 2, 12)
        },
        {
            id: 'e3',
            title: '테스트',
            amount: 294.67,
            date: new Date(2021, 2, 28),
        },
        {
            id: 'e4',
            title: '히힛',
            amount: 450,
            date: new Date(2021, 5, 12),
        },
		{
            id: 'e5',
            title: 'ㅎㅎ',
            amount: 450,
            date: new Date(2021, 5, 12),
        },
    ] );

	  const addExpenseHandler = (expense) => {
		setExpenses((prevExpense) => {
		  return [expense, ...prevExpense];
		});
	  };


	return (
		<div className="App">
				<NewExpense onAddExpense={addExpenseHandler} />
				<Expenses items={expenses} />
		</div>
	);
}

export default App;