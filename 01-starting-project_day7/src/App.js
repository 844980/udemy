import React, { useState } from'react';

import UserList from './components/User/UserList/UserList';
import UserInput from './components/User/UserInput/UserInput';

function App() {
	const [userList, setUserList] = useState([]);
	const onSubmitHandler = (userItem)=>{
		setUserList((prevUserList)=>{return [
			...prevUserList,
			userItem
			];	
		});
	};
  return (
    <div>
		<UserInput onSubmit = {onSubmitHandler}/>
		<UserList userList = {userList}/>
    </div>
  );
}

export default App;
