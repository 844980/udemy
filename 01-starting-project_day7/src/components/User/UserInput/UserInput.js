import styles from './UserInput.Module.css';
import Button from '../../UI/Button/Button';
import Card from '../../UI/Card/Card';
import Modal from '../../UI/Modal/Modal';
import { useState } from'react';


const UserInput = (props)=>{
	const userItemClear = ()=>{
		return {
			userName : '',
			userAge : '',
		};
	}
	const [modal, setModal] = useState(null);
	const [userItem, setUserItem] = useState(userItemClear());
	
	const submitHandler = (event) => {
		event.preventDefault();
		console.log(userItem.userName);
		if (userItem.userName.trim().length === 0) {
		  setModal({
			title: '안내',
			message: '이름을 입력해주세요',
		  });
		  return;
		}
		if (userItem.userAge.trim().length === 0) {
		  setModal({
			title: '안내',
			message: '나이을 입력해주세요',
		  });
		  return;
		}
		setUserItem({
			...userItem,
			id : Math.random().toString()
		});
		props.onSubmit(userItem);
		setUserItem(userItemClear());
	}
	
	const showModal = (param) =>{
		
	}
	
	const userNameChangeHandler = (event) =>{
		setUserItem({
			...userItem,
			userName : event.target.value
		});
	}
	
	const userAgeChangeHandler = (event) =>{
		setUserItem({
			...userItem,
			userAge : event.target.value
		});
	}
	
	const modalHandler = (event) => {
		setModal(null);
	}
	
	return (
		<>
		{
		modal && (
		<Modal title={modal.title} message={modal.message} onConfirm={modalHandler}/>
		)
		}
		<Card>
			<form onSubmit={submitHandler}>
			<div>
				<div>이름</div>
				<input id='userName' value={userItem.userName} type = 'text' onChange={userNameChangeHandler}></input>
			</div>
			<div>
				<div>나이</div>
				<input id='userAge'  value={userItem.userAge} type = 'number' onChange={userAgeChangeHandler}></input>
			</div>
			<Button type="submit">추가</Button>
		</form>
		</Card>
		</>
	);
};

export default UserInput;