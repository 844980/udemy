import styles from './UserList.Module.css';
import UserItem from '../UserItem/UserItem';
import Card from '../../UI/Card/Card';

const UserList = (props) => {
	return (
	<Card>
	<div className="UserList">
		 { props.userList.map(user => (
			<UserItem key="{user.id}" user={user} />
		  	)
			)
		  }
	</div>
	</Card>
	);
}

export default UserList;