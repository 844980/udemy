import style from './UserItem.Module.css'

const UserItem = (props) =>{
	return (
		<div>
			{props.user.userName} ({props.user.userAge})
		</div>
	);
};

export default UserItem;