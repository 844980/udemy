import styles from './Card.Module.css';

const Card = (props) => {
  return <div className={`${styles.card} ${props.className}`}>{props.children}</div>;
};

export default Card;